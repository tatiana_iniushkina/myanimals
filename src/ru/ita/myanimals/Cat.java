package ru.ita.myanimals;

/**
 * Created by ITA on 16.10.2014.
 */
public class Cat implements Animals {
    @Override
    public String getName() {
        return "Кошка";
    }

    @Override
    public String getVoice() {
        return "мяу-мяу";
    }
}
