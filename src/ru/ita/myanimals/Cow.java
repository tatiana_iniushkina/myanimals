package ru.ita.myanimals;

/**
 * Created by ITA on 21.10.2014.
 */
public class Cow implements Animals {
    @Override
    public String getName() {
        return "Корова";
    }

    @Override
    public String getVoice() {
        return "му-му";
    }
}
