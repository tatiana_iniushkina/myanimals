package ru.ita.myanimals;

/**
 * Created by ITA on 21.10.2014.
 */
public class Frog implements Animals {
    @Override
    public String getName() {
        return "Лягушка";
    }

    @Override
    public String getVoice() {
        return "ква-ква";
    }
}
