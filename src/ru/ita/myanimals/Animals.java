package ru.ita.myanimals;

/**
 * Created by ITA on 16.10.2014.
 */
public interface Animals {
    public String getName();
    public String getVoice();
}
