package ru.ita.myanimals;

/**
 * Created by ITA on 16.10.2014.
 */
public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog();
        Cat cat = new Cat();
        Cow cow = new Cow();
        Frog frog = new Frog();
        print(dog);
        print(cat);
        print(cow);
        print(frog);
    }


    public static void print(Animals animal){
        System.out.println(animal.getName() +
            " говорит " +
            animal.getVoice());
    }
}
